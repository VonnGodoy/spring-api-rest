package br.gov.in.sig.sipac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.in.sig.sipac.entidade.FinalidadeLevantamento;
import br.gov.in.sig.sipac.services.FinalidadeLevantamentoService;

@RestController
@RequestMapping("/finalidade")
public class FinalidadeLevantamentoController {

    @Autowired
    FinalidadeLevantamentoService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FinalidadeLevantamento> listarFinalidades() {
        return service.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FinalidadeLevantamento buscarPorId(@PathVariable Integer id) {
        return service.findById(id);
    }
}
