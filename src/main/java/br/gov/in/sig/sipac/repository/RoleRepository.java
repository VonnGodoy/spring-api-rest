package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findById(Integer id);

    List<Role> findAll();
}
