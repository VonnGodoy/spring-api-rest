package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.ItemLevantamento;
import br.gov.in.sig.sipac.entidade.Levantamento;

@Repository
public interface ItemLevantamentoRepository extends JpaRepository<ItemLevantamento, Integer> {

    ItemLevantamento findById(Integer id);

    List<ItemLevantamento> findAll();

    List<ItemLevantamento> findByLevantamento(Levantamento levatamento);

}
