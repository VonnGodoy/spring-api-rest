package br.gov.in.sig.sipac.entidade;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Table(schema = "comum")
@Entity(name = "unidade")
public class Unidade {

    @Id
    @Column(name = "id_unidade")
    private Integer id;

    @Column(name = "unidade_orcamentaria")
    private Boolean unidadeOrcamentaria;

    @Column(name = "unidade_responsavel")
    private Integer unidadeResponsavel;

    @Column(name = "id_responsavel")
    private Integer responsavel;

    @Column(name = "nome")
    private String nome;

    @Column(name = "codigo_unidade")
    private BigInteger codUnidade;

    @Column(name = "sigla")
    private String sigla;

    @Column(name = "tipo")
    private Integer tipo;

    @Column(name = "compradora")
    private Boolean compradora;

    @Column(name = "compradora_engenharia")
    private Boolean compradoraEngenharia;

    @Column(name = "categoria")
    private String categoria;

    @Column(name = "telefones")
    private String telefones;

    @Column(name = "hierarquia")
    private String hierarquia;

    @Column(name = "nome_capa")
    private String nomeCapa;

    @Column(name = "sipac")
    private Boolean sipac;

    @Column(name = "sequencia_modalidade_compra")
    private Integer sequenciaModalidadeCompra;

    @Column(name = "presidente_comissao")
    private String presidenteComissao;

    @Column(name = "prazo_envio_bolsa_inicio")
    private Integer prazoEnvioBolsaInicio;

    @Column(name = "prazo_envio_bolsa_fim")
    private Integer prazoEnvioBolsaFim;

    @Column(name = "id_gestora")
    private Integer gestora;

    @Column(name = "template_parecer_dl")
    private Integer templateParecerDL;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;

    @Column(name = "cnpj")
    private BigInteger cnpj;

    @Column(name = "id_gestora_academica")
    private Integer gestoraAcademica;

    @Column(name = "tipo_academica")
    private Integer tipoAcademica;

    @Column(name = "unidade_academica")
    private Boolean unidadeAcademica;

    @Column(name = "sigla_academica")
    private String sgAcademica;

    @Column(name = "codigoSiapecad")
    private BigInteger codSiapecad;

    @Column(name = "email")
    private String email;

    @Column(name = "data_criacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCriacao;

    @Column(name = "data_extincao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtExtincao;

    @Column(name = "gestora_frequencia")
    private Boolean gestoraFrequencia;

    @Column(name = "organizacional")
    private Boolean organizacional;

    @Column(name = "id_tipo_organizacional")
    private Integer tpOrganizacional;

    @Column(name = "funcao_remunerada")
    private Boolean funcaoRemunerada;

    @Column(name = "tipo_funcao_remunerada")
    private Integer tpFuncaoRemunerada;

    @Column(name = "hierarquia_organizacional")
    private String hierarquiaOrganizacional;

    @Column(name = "id_area_atuacao")
    private Integer areaAtuacao;

    @Column(name = "id_ambiente_organizacional")
    private Integer ambienteOrganizacional;

    @Column(name = "avaliacao")
    private Boolean avaliacao;

    @Column(name = "id_unid_resp_org")
    private Integer unidRespOrg;

    @Column(name = "metas")
    private Boolean metas;

    @Column(name = "submete_proposta_extensao")
    private Boolean submetePropostaExtensao;

    @Column(name = "id_classificacao_unidade")
    private Integer classificacaoUnidade;

    @Column(name = "id_nivel_organizacional")
    private Integer nivelOrganizacional;

    @Column(name = "nome_ascii")
    private String nomeAscii;

    @Column(name = "id_usuario_cadastro")
    private Integer usuarioCadastro;

    @Column(name = "codigo_unidade_gestora_siafi")
    private Integer codigoUnidadeGestoraSiafi;

    @Column(name = "codigo_gestao_siafi")
    private Integer codGestaoSiafi;

    @Column(name = "ativo")
    private Boolean ativo;

    @Column(name = "id_municipio")
    private Integer municipio;

    @Column(name = "endereco")
    private String endereco;

    @Column(name = "uf")
    private String uf;

    @Column(name = "cep")
    private String cep;

    @Column(name = "permite_gestao_centros_gestora_superior")
    private Boolean permiteGestaoCentrosGestoraSuperior;

    @Column(name = "protocolizadora")
    private Boolean protocolizadora;

    @Column(name = "radical")
    private Integer radical;

    @Column(name = "codigo_siorg")
    private Integer codSiorg;

    @Column(name = "id_tipo_turno")
    private Integer tpTurno;
}
