package br.gov.in.sig.sipac.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(schema = "patrimonio")
@Entity(name = "item_levantamento")
public class ItemLevantamento {

    @Id
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "id_levantamento", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private Levantamento levantamento;

    @Column(name = "num_tombamento")
    private Integer nuTombamento;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "id_marca")
    private Integer marca;

    @JoinColumn(name = "id_bem", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private Bem bem;

    @Column(name = "estado_bem")
    private Integer estadoBem;

    @JoinColumn(name = "id_usuario")
    @OneToOne(fetch = FetchType.EAGER)
    private Usuario usuario;

    @Column(name = "data_cadastro")
    private Date dtcadastro;

}
