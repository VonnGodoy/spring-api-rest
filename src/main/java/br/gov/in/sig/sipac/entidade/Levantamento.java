
package br.gov.in.sig.sipac.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Table(schema = "patrimonio")
@Entity(name = "levantamento_patrimonial")
public class Levantamento {

    @Id
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "id_local_unidade", referencedColumnName = "id_local")
    @OneToOne(fetch = FetchType.EAGER)
    private Localidade local;

    @JoinColumn(name = "id_usuario")
    @OneToOne(fetch = FetchType.EAGER)
    private Usuario usuario;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro")
    private Date dtCadastro;

    @Column(name = "status")
    private Integer status;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "id_registro_estorno")
    private Integer registroEstorno;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_estorno")
    private Date dtEstorno;

    @JoinColumn(name = "tipo_finalidade", referencedColumnName = "id_finalidade")
    @OneToOne(fetch = FetchType.EAGER)
    private FinalidadeLevantamento finalidade;

    @Column(name = "ano")
    private Integer ano;

    @Column(name = "numero")
    private Integer numero;

}