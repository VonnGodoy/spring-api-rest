package br.gov.in.sig.sipac.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.in.sig.sipac.entidade.Usuario;
import br.gov.in.sig.sipac.repository.UsuarioRepository;

@Service
@Transactional
public class UsuarioService {
    @Autowired
    private UsuarioRepository userRepository;

    @Transactional(readOnly = true)
    public Usuario findByLogin(String login) {
        return userRepository.findByLogin(login);

    }

    @Transactional(readOnly = true)
    public Usuario findById(Integer id) {
        return userRepository.findById(id);

    }

}
