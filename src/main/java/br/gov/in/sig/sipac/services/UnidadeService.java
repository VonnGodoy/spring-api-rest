package br.gov.in.sig.sipac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.in.sig.sipac.entidade.Unidade;
import br.gov.in.sig.sipac.repository.UnidadeRepository;

@Service
@Transactional
public class UnidadeService {

    @Autowired
    private UnidadeRepository reposotory;

    public List<Unidade> findAll() {
        return reposotory.findAll();
    }

    public List<Unidade> findBySiglaContaining(String sigla) {
        return reposotory.findBySiglaContaining(sigla);
    }

    public List<Unidade> findByNomeContaining(String nome) {
        return reposotory.findByNomeContaining(nome);
    }

    public Unidade findById(Integer id) {
        return reposotory.findById(id);
    }

    public List<Unidade> findByHierarquia(String hierarquia) {
        return reposotory.findByHierarquiaOrganizacionalContaining(hierarquia);
    }

    public List<Unidade> findByUnidadeResponsavel(Integer unidadeResponsavel) {
        return reposotory.findByUnidadeResponsavel(unidadeResponsavel);
    }

}
