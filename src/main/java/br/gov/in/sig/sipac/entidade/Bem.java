package br.gov.in.sig.sipac.entidade;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Table(schema = "patrimonio")
@Entity(name = "bem")
public class Bem {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "num_tombamento")
    private Integer nuTombamento;

    @Column(name = "denominacao")
    private String denominacao;

    @Column(name = "especificacao")
    private String especificacao;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "estado_bem")
    private Integer estadoBem;

    @Column(name = "ano_balanco")
    private Integer anoBalanco;

    @Column(name = "garantia")

    @Temporal(TemporalType.TIMESTAMP)
    private Date garantia;

    @Column(name = "status")
    private Integer status;

    @Column(name = "id_material")
    private Integer material;

    @Column(name = "id_usuario")
    private Integer usuario;

    @Column(name = "id_fornecedor")
    private Integer fornecedor;

    @Column(name = "id_marca_produto")
    private Integer marcaProduto;

    @Column(name = "id_termo_responsabilidade")
    private Integer termoResponsabilidade;

    @Column(name = "id_tipo_documento")
    private Integer tipoDocumento;

    @Column(name = "numero_doc_doacao")
    private Integer numeroDocDoacao;

    @Column(name = "ano_doc_doacao")
    private Integer anoDocDoacao;

    @Column(name = "ano_tombamento_fundacao")
    private Integer anoTombamentoFundacao;

    @Column(name = "numero_tombamento_fundacao")
    private Integer nuTombamentoFundacao;

    @JoinColumn(name = "id_unidade_resp_atual", referencedColumnName = "id_unidade")
    @OneToOne(fetch = FetchType.EAGER)
    private Unidade unidadeRespAtual;

    @Column(name = "tipo_tombamento")
    private Integer tpTombamento;

    @Column(name = "id_item_nota_fiscal")
    private Integer itemNotaFiscal;

    @Column(name = "status_movimentacao")
    private Integer statusMovimentacao;

    @Column(name = "id_doador")
    private Integer doador;

    @Column(name = "id_finalidade_bem")
    private Integer finalidadeBem;

    @Column(name = "data_balanco")

    @Temporal(TemporalType.TIMESTAMP)
    private Date dtBalanco;

    @Column(name = "valor")
    private BigDecimal valor;

    @Column(name = "itens_colecao")
    private Integer itensColecao;

    @Column(name = "anulado")
    private Boolean anulado;

    @JoinColumn(name = "id_local")
    @OneToOne(fetch = FetchType.EAGER)
    private Localidade local;

    @Column(name = "id_bem_pai")
    private Integer bemPai;

    @Column(name = "prazo_garantia")
    private Integer prazoGarantia;

    @Column(name = "data_documento")

    @Temporal(TemporalType.TIMESTAMP)
    private Date dtDocumento;

    @Column(name = "id_tipo_entrada")
    private Integer tpEntrada;

    @Column(name = "agrega_itens_nota")
    private Boolean agregaItensNota;

    @Column(name = "denominacao_ascii")
    private String denominacaoAscii;

    @Column(name = "id_indisponibilidade_bem")
    private Integer indisponibilidadeBem;

    @Column(name = "id_ug_tombamento")
    private Integer ugTombamento;

    @Column(name = "bem_terceiros")
    private Boolean bemTerceiros;

    @Column(name = "valor_bem_terceiros")
    private BigDecimal valorBemTerceiros;

    @Column(name = "num_processo")
    private String nuProcesso;

    @Column(name = "id_processo")
    private Integer processo;

    @Column(name = "valor_acumulado")
    private BigDecimal valorAcumulado;

    @Column(name = "valor_entrada")
    private BigDecimal valorEntrada;

    @Column(name = "data_ultimo_ajuste")

    @Temporal(TemporalType.TIMESTAMP)
    private Date dtUltimoAjuste;

    @Column(name = "valor_depreciacao_acumulada")
    private BigDecimal valorDepreciacaoAcumulada;

    @Column(name = "num_cod_barra")
    private Integer nuCodBarra;

    @Column(name = "version_number")
    private Integer versionNumber;

    @Column(name = "depreciavel")
    private String depreciavel;

    @Column(name = "data_inicio_uso")

    @Temporal(TemporalType.TIMESTAMP)
    private Date dtInicioUso;

    @Column(name = "vida_util")
    private Integer vidaUtil;

    @Column(name = "vida_util_base_depreciacao")
    private Integer vidaUtilBaseDepreciacao;

    @Column(name = "valor_residual")
    private Integer valorResidual;

    @Column(name = "data_ultimo_ajuste_depreciacao")

    @Temporal(TemporalType.TIMESTAMP)
    private Date dtUltimoAjusteDepreciacao;

    @Column(name = "data_base_depreciacao")

    @Temporal(TemporalType.TIMESTAMP)
    private Date dtBaseDepreciacao;

    @Column(name = "codmerg")
    private String codmerg;

    @Column(name = "id_unidade_legado")
    private Integer unidadeLegado;

    @Column(name = "num_processo_tombamento")
    private String nuProcessoTombamento;
}
