package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.Localidade;
import br.gov.in.sig.sipac.entidade.Unidade;

@Repository
public interface LocalidadeRepository extends JpaRepository<Localidade, Integer> {

    Localidade findById(Integer id);

    List<Localidade> findAll();

    List<Localidade> findByUnidade(Unidade unidade);
}
