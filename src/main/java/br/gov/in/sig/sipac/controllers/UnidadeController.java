package br.gov.in.sig.sipac.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.in.sig.sipac.entidade.Unidade;
import br.gov.in.sig.sipac.services.UnidadeService;

@RestController
@RequestMapping("/unidade")
public class UnidadeController {

    private static final Logger logger = LoggerFactory.getLogger(UnidadeController.class);

    @Autowired
    UnidadeService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Unidade> listarUnidades() {
        return service.findAll();
    }

    @RequestMapping(value = "/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Unidade buscarPorId(@PathVariable Integer idUnidade) {
        return service.findById(idUnidade);
    }

    @RequestMapping(value = "/like/{texto}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Unidade> buscarPorLike(@PathVariable String texto) {

        List<Unidade> unidades = new ArrayList<Unidade>();

        unidades.addAll(service.findBySiglaContaining(texto));
        unidades.addAll(service.findByNomeContaining(texto));

        return unidades;
    }

    @RequestMapping(value = "/listarPorResponsavel/{unidadeResponsavel}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Unidade> buscarPorUnidadeResponsavel(@PathVariable Integer unidadeResponsavel) {
        return service.findByUnidadeResponsavel(unidadeResponsavel);
    }

}
