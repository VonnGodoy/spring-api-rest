package br.gov.in.sig.sipac.utils;

import java.util.HashMap;

@SuppressWarnings("serial")
public class NegocioException extends RuntimeException {

    private HashMap<String, String[]> erros = new HashMap<String, String[]>();

    /**
     * 
     * @Method Construtor Padrao
     * @Return NegocioException
     * 
     **/
    public NegocioException() {
        super();
    }

    /**
     * 
     * @Method Construtor Padrao
     * @param Throwable e
     * @Return NegocioException
     * 
     **/
    public NegocioException(Throwable e) {
        super(e);
    }

    /**
     * 
     * @Method Construtor Padrao
     * @param Throwable e
     * @param String coderro
     * @param String parametros
     * @Return NegocioException
     * 
     **/
    public NegocioException(Throwable e, String coderro, String... parametros) {
        super(e.getMessage(), e);
        erros.put(coderro, parametros);
    }

    /**
     * 
     * @Method Construtor Padrao
     * @param String coderro
     * @param String parametros
     * @Return NegocioException
     * 
     **/
    public NegocioException(String coderro, String... parametros) {
        super(coderro);
        erros.put(coderro, parametros);
    }

    /**
     * 
     * @Method Construtor Padrao
     * @param String message
     * @Return NegocioException
     * 
     **/
    public NegocioException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 
     * @Method addErro
     * @param String coderro
     * @param String parametros
     * @Return
     * 
     **/
    public void addErro(String coderro, String... parametros) {
        erros.put(coderro, parametros);
    }

    /**
     * 
     * @Method addErro
     * @param String coderro
     * @Return
     * 
     **/
    public void addErro(String coderro) {
        erros.put(coderro, new String[] {});
    }

    /**
     * 
     * @Method getErros
     * @Return HashMap
     * 
     **/
    public HashMap<String, String[]> getErros() {
        return erros;
    }
}