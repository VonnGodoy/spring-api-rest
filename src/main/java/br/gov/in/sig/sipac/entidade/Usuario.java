package br.gov.in.sig.sipac.entidade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Table(schema = "comum")
@Entity(name = "usuario")
public class Usuario implements Serializable {

    @Id
    @Column(name = "id_usuario")
    private Integer id;

    @Column(name = "expira_senha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiraSenha;

    @Column(name = "login")
    private String login;

    @JoinColumn(name = "id_unidade")
    @OneToOne(fetch = FetchType.EAGER)
    private Unidade unidade;

    @Column(name = "funcionario")
    private boolean funcionario;

    @Column(name = "ultimo_acesso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimoAcesso;

    @Column(name = "email")
    private String email;

    @Column(name = "inativo")
    private boolean inativo;

    @Column(name = "id_setor")
    private Integer setor;

    @Column(name = "id_pessoa")
    private Integer pessoa;

    @Column(name = "ano_orcamentario")
    private Integer anoOrcamentario;

    @Column(name = "id_aluno")
    private Integer aluno;

    @Column(name = "id_servidor")
    private Integer servidor;

    @Column(name = "autorizado")
    private boolean autorizado;

    @Column(name = "ramal")
    private String ramal;

    @Column(name = "objetivo_cadastro")
    private String objetivoCadastro;

    @Column(name = "id_restaurante")
    private Integer restaurante;

    @Column(name = "usuario_protocolo")
    private String usuarioProtocolo;

    @Column(name = "usuario_dmp")
    private String usuarioDMP;

    @Column(name = "justificativa_negado")
    private String justificativaNegado;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "origem_cadastro")
    private String origemCadastro;

    @Column(name = "id_foto")
    private Integer foto;

    @Column(name = "senha")
    @Basic(fetch = FetchType.LAZY)
    private String senha;

    @Column(name = "id_consignataria")
    private Integer consignataria;

    @Column(name = "id_plano_saude")
    private Integer planoSaude;

}
