package br.gov.in.sig.sipac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class MobileAPP extends SpringBootServletInitializer {

    /*
     * @Override protected SpringApplicationBuilder configure(SpringApplicationBuilder application) { return
     * application.sources(MobileAPP.class); }
     */

    public static void main(String[] args) {
        SpringApplication.run(MobileAPP.class, args);
    }
}