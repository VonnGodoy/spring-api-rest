package br.gov.in.sig.sipac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.in.sig.sipac.entidade.Role;
import br.gov.in.sig.sipac.repository.RoleRepository;

@Service
@Transactional
public class RoleService {

    @Autowired
    private RoleRepository reposotory;

    public Role findById(Integer id) {
        return reposotory.findById(id);
    }

    public List<Role> findAll() {
        return reposotory.findAll();
    }
}
