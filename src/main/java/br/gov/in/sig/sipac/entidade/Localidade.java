package br.gov.in.sig.sipac.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(schema = "patrimonio")
@Entity(name = "local_bem")
public class Localidade {

    @Id
    @Column(name = "id_local")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @JoinColumn(name = "id_unidade")
    @OneToOne(fetch = FetchType.EAGER)
    private Unidade unidade;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "ativo")
    private Boolean ativo;

    @Column(name = "descricao_ascii")
    private String descricaoAscii;

    @Column(name = "id_registro_entrada")
    private Integer registroEntrada;

    @Column(name = "data_cadastro")
    private Date dtcadastro;

    @Column(name = "observacao")
    private String observacao;
}
