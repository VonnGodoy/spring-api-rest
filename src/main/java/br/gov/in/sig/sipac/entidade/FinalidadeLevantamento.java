package br.gov.in.sig.sipac.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(schema = "patrimonio")
@Entity(name = "tipo_finalidade_levantamento")
public class FinalidadeLevantamento {

    @Id
    @Column(name = "id_finalidade")
    private Integer id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "ativo")
    private Boolean ativo;
}
