package br.gov.in.sig.sipac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.in.sig.sipac.entidade.Bem;
import br.gov.in.sig.sipac.entidade.Localidade;
import br.gov.in.sig.sipac.repository.BemRepository;

@Service
@Transactional
public class BemService {

    @Autowired
    private BemRepository repository;

    public Bem findById(Integer id) {
        return repository.findById(id);
    }

    public List<Bem> findAll() {
        return repository.findAll();
    }

    public List<Bem> findByLocalidade(Localidade local) {
        return repository.findByLocal(local);
    }
}
