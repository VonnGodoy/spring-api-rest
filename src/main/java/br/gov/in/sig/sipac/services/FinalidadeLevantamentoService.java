package br.gov.in.sig.sipac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.in.sig.sipac.entidade.FinalidadeLevantamento;
import br.gov.in.sig.sipac.repository.FinalidadeLevantamentoRepository;

@Service
@Transactional
public class FinalidadeLevantamentoService {

    @Autowired
    private FinalidadeLevantamentoRepository repository;

    public FinalidadeLevantamento findById(Integer id) {
        return repository.findById(id);
    }

    public List<FinalidadeLevantamento> findAll() {
        return repository.findAll();
    }

}
