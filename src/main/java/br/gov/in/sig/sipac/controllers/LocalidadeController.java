package br.gov.in.sig.sipac.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.in.sig.sipac.entidade.Localidade;
import br.gov.in.sig.sipac.entidade.Unidade;
import br.gov.in.sig.sipac.services.LocalidadeService;

@RestController
@RequestMapping("/localidade")
public class LocalidadeController {

    private static final Logger logger = LoggerFactory.getLogger(BemController.class);

    @Autowired
    private LocalidadeService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Localidade> listarLocalidades() {
        return service.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Localidade buscarPorId(@PathVariable Integer id) {
        return service.findById(id);
    }

    @RequestMapping(value = "/listarPorUnidade/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Localidade> listarPorUnidade(@PathVariable Integer id) {
        Unidade unidade = new Unidade();
        unidade.setId(id);
        return service.findByUnidade(unidade);
    }
}
