package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.FinalidadeLevantamento;
import br.gov.in.sig.sipac.entidade.Levantamento;
import br.gov.in.sig.sipac.entidade.Localidade;
import br.gov.in.sig.sipac.entidade.Usuario;

@Repository
public interface LevantamentoRepository extends JpaRepository<Levantamento, Integer> {

    Levantamento findById(Integer id);

    List<Levantamento> findAll();

    List<Levantamento> findByLocal(Localidade local);

    List<Levantamento> findByUsuario(Usuario usuario);

    List<Levantamento> findByFinalidade(FinalidadeLevantamento finalidade);

}
