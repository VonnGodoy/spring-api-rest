package br.gov.in.sig.sipac.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.in.sig.sipac.entidade.Localidade;
import br.gov.in.sig.sipac.entidade.Unidade;
import br.gov.in.sig.sipac.repository.LocalidadeRepository;

@Service
@Transactional
public class LocalidadeService {

    @Autowired
    private LocalidadeRepository repository;

    public Localidade findById(Integer id) {
        return repository.findById(id);
    }

    public List<Localidade> findAll() {
        return repository.findAll();
    }

    public List<Localidade> findByUnidade(Unidade unidade) {
        return repository.findByUnidade(unidade);
    }
}
