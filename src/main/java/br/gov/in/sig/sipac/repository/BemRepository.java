package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.Bem;
import br.gov.in.sig.sipac.entidade.Localidade;

@Repository
public interface BemRepository extends JpaRepository<Bem, String> {

    Bem findById(Integer id);

    List<Bem> findAll();

    List<Bem> findByLocal(Localidade local);

}
