package br.gov.in.sig.sipac.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.in.sig.sipac.entidade.Bem;
import br.gov.in.sig.sipac.entidade.Localidade;
import br.gov.in.sig.sipac.services.BemService;

@RestController
@RequestMapping("/bem")
public class BemController {

    private static final Logger logger = LoggerFactory.getLogger(BemController.class);

    @Autowired
    private BemService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Bem> listarBens() {
        return service.findAll();
    }

    @RequestMapping(value = "/{idBem}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Bem buscarPorId(@PathVariable Integer idBem) {
        return service.findById(idBem);
    }

    @RequestMapping(value = "/listarPorLocalidade/{localidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Bem> listarPorlocalidade(@PathVariable Localidade localidade) {
        return service.findByLocalidade(localidade);
    }

}
