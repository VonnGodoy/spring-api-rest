package br.gov.in.sig.sipac.entidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity(name = "permissao")
public class Role {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_sistema")
    private Long sistema;

    @Column(name = "id_usuario")
    private Long usuario;

    @Column(name = "id_papel")
    private Long papel;

    @Column(name = "id_atribuidor")
    private Long Atribuidor;

    @Column(name = "data_atribuicao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAtribuicao;

    @Column(name = "data_expiracao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtExpiracao;

    @Column(name = "codmerg")
    private Long codmerg;

    @Column(name = "id_unidade_papel")
    private Long unidadePapel;

    @Column(name = "numero_chamado")
    private Long nuChamado;

    @Column(name = "motivo")
    private String motivo;

    @Column(name = "autorizada")
    private boolean autorizada;

    @Column(name = "id_designacao")
    private Long designacao;
}
