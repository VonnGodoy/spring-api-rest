package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.FinalidadeLevantamento;

@Repository
public interface FinalidadeLevantamentoRepository extends JpaRepository<FinalidadeLevantamento, Integer> {

    FinalidadeLevantamento findById(Integer id);

    List<FinalidadeLevantamento> findAll();
}
