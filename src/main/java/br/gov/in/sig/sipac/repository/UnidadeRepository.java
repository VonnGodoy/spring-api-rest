package br.gov.in.sig.sipac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.in.sig.sipac.entidade.Unidade;

@Repository
public interface UnidadeRepository extends JpaRepository<Unidade, Integer> {

    List<Unidade> findAll();

    Unidade findById(Integer unidade);

    List<Unidade> findBySiglaContaining(String sigla);

    List<Unidade> findByNomeContaining(String nome);

    List<Unidade> findByUnidadeResponsavel(Integer unidadeResponsavel);

    List<Unidade> findByHierarquiaOrganizacionalContaining(String hierarquiaOrganizacional);

}
